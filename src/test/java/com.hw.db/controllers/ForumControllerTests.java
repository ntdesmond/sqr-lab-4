package com.hw.db.controllers;

import com.hw.db.DAO.*;
import com.hw.db.models.*;

import org.mockito.Mockito;
import org.mockito.MockedStatic;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

// Copied from README with fixes to make it fucking run
class ForumControllerTests {
    private User loggedIn;
    private Forum toCreate;

    @BeforeEach
    @DisplayName("forum creation test")
    void createForumTest() {
        loggedIn = new User("some","some@email.mu", "name", "nothing");
        toCreate = new Forum(12, "some", 3, "title", "some");
    }

    @Test
    @DisplayName("Correct forum creation test")
    void correctlyCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic<ForumDAO> forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                forumController controller = new forumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(toCreate), controller.create(toCreate), "Result for succeeding forum creation");
            }
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }

    @Test
    @DisplayName("Not authorithed user fails to create forum")
    void noLoginWhenCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            //userMock.when(() -> UserDAO.Search("some")).thenReturn(null);
            userMock.when(() -> UserDAO.Search("some")).thenThrow(new EmptyResultDataAccessException(1));
            forumController controller = new forumController();

            // Did you EVER run the tests? Your 'Message' objects don't compare to each other directly

            // controller.create(toCreate);
            ResponseEntity response = controller.create(toCreate);

            // assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Владелец форума не найден.")), controller.create(toCreate), "Result when unauthorised user tried to create a forum");
            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode(), "Result when unauthorised user tried to create a forum");
            assertEquals("Владелец форума не найден.", ((Message) Objects.requireNonNull(response.getBody())).getData(), "Result when unauthorised user tried to create a forum");
        }
    }

    @Test
    @DisplayName("User fails to create forum because it already exists")
    void conflictWhenCreatesForumButSomeoneFuckedUpTheTestFunctionName() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic<ForumDAO> forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                forumDAO.when(() -> ForumDAO.CreateForum(toCreate)).thenThrow(new DuplicateKeyException("Forum already exists"));
                forumController controller = new forumController();

                // And yet another missed line bruh
                forumDAO.when(() -> ForumDAO.Info(toCreate.getSlug())).thenReturn(toCreate);

                assertEquals(ResponseEntity.status(HttpStatus.CONFLICT).body(toCreate), controller.create(toCreate), "Result for conflict while forum creation");
            }
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }

    @Test
    @DisplayName("User fails to create forum because server cannot access DB")
    void conflictWhenCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic<ForumDAO> forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                //forumDAO.when(() -> ForumDAO.CreateForum(toCreate)).thenThrow(new DataAccessException("Forum already exists"));
                //                                                               ^ come on it's a fucking ABSTRACT class here
                forumDAO.when(() -> ForumDAO.CreateForum(toCreate)).thenThrow(new DataAccessResourceFailureException("No db sorry"));
                forumController controller = new forumController();

                // controller.create(toCreate);
                ResponseEntity response = controller.create(toCreate);

                // assertEquals(ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new Message("Что-то на сервере.")), controller.create(toCreate), "Result for DB error while server creation");
                assertEquals(HttpStatus.NOT_ACCEPTABLE, response.getStatusCode(), "Result for DB error while server creation");
                assertEquals("Что-то на сервере.", ((Message) Objects.requireNonNull(response.getBody())).getData(), "Result for DB error while server creation");

            }
            // what have you tried to do with that? it's literally calling a mock defined above
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }

    @Test
    @DisplayName("User gets list of threads test #1")
    void ThreadListTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug",null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"), Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }
    // Other tests were was have been or not written on the lesson i don't care anyway

}

