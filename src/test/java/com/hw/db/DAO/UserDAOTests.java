package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.stream.Stream;

import static org.mockito.Mockito.mock;

class UserDAOTests {
    @Nested
    @DisplayName("User.Change() tests")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class ChangeTests {
        Stream<Arguments> noUpdateArguments() {
            return Stream.of(
                Arguments.of(new User(null, null, null, null)),
                Arguments.of(new User("user", null, null, null))
            );
        }

        @ParameterizedTest
        @DisplayName("Change produces proper SQL when username == null or no updates passed")
        @MethodSource("noUpdateArguments")
        void testChangeNoOp(User user) {
            JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
            new UserDAO(mockJdbc);
            UserDAO.Change(user);
            Mockito.verifyNoInteractions(mockJdbc);
        }


        Stream<Arguments> updatingArguments() {
            return Stream.of(
                    Arguments.of(new User("user", null, "name", null), "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                    Arguments.of(new User("user", null, null, "some"), "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
                    Arguments.of(new User("user", null, "name", "some"), "UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                    Arguments.of(new User("user", "email", null, null), "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                    Arguments.of(new User("user", "email", "name", null), "UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                    Arguments.of(new User("user", "email", null, "some"), "UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
                    Arguments.of(new User("user", "email", "name", "some"), "UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;")
            );
        }

        @ParameterizedTest
        @DisplayName("Change produces proper SQL when username != null and there are updates")
        @MethodSource("updatingArguments")
        void testChangeSQL(User user, String sql) {
            JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
            new UserDAO(mockJdbc);
            UserDAO.Change(user);
            Mockito
                .verify(mockJdbc)
                .update(
                    Mockito.eq(sql),
                    (Object)Mockito.any()
                );
        }
    }
}

