package com.hw.db.DAO;

import com.hw.db.models.Post;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.util.stream.Stream;

import static org.mockito.Mockito.mock;

class PostDAOTests {
    @Nested
    @DisplayName("setPost tests")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class SetPostTests {
        Post originalPost = new Post("user", new Timestamp(1), "forum", "text1", 0, 0, false);
        Stream<Arguments> arguments() {
            return Stream.of(
                Arguments.of(new Post("user", new Timestamp(2), "forum", "text1", 0, 0, false), "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(new Post("user", new Timestamp(2), "forum", "text2", 0, 0, false), "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(new Post("user2", new Timestamp(1), "forum", "text1",0, 0, false), "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Arguments.of(new Post("user2", new Timestamp(2), "forum", "text2",0, 0, false), "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;")
            );
        }

        @ParameterizedTest
        @DisplayName("setPost produces proper SQL")
        @MethodSource("arguments")
        void testSetPostSQL(Post post, String sql) {
            JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
            Mockito.when(
                mockJdbc.queryForObject(
                    Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                    Mockito.any(PostDAO.PostMapper.class),
                    Mockito.eq(1)
                )
            ).thenReturn(originalPost);
            new PostDAO(mockJdbc);
            PostDAO.setPost(1, post);
            Mockito.verify(mockJdbc).update(Mockito.eq(sql), (Object)Mockito.any());
        }

        @Test
        @DisplayName("setPost does nothing if the post is the same")
        void testSetPostDoesNothing() {
            JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
            Mockito.when(
                mockJdbc.queryForObject(
                    Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                    Mockito.any(PostDAO.PostMapper.class),
                    Mockito.eq(1)
                )
            ).thenReturn(originalPost);
            new PostDAO(mockJdbc);
            PostDAO.setPost(1, originalPost);
            Mockito.verify(mockJdbc, Mockito.never()).update(Mockito.any(), (Object)Mockito.any());
        }
    }
}

