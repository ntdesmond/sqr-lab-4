package com.hw.db.DAO;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.stream.Stream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ForumDAOTests {
    // Copied from README
    @Test
    @DisplayName("User gets list of threads test #1")
    void ThreadListTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug",null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"), Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }
    // Other tests were was have been or not written on the lesson i don't care anyway

    @Nested
    @DisplayName("UserList tests")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class UserListTests {
        Stream<Arguments> arguments() {
            return Stream.of(
                Arguments.of("", 5, "", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Arguments.of("", null, null, false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
                Arguments.of("", null, "", false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;")
            );
        }

        @ParameterizedTest(name = "limit={1}, since={2}, desc={3}")
        @DisplayName("UserList produces proper SQL")
        @MethodSource("arguments")
        void testUserListSQL(String slug, Number limit, String since, Boolean desc, String sql) {
            JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
            new ForumDAO(mockJdbc); // are you kidding? assigning to STATIC via constructor?
            ForumDAO.UserList(slug, limit, since, desc);
            Mockito
                .verify(mockJdbc)
                .query(
                    Mockito.eq(sql),
                    Mockito.any(Object[].class),
                    Mockito.any(UserDAO.UserMapper.class)
                );
        }
    }
}

