package com.hw.db.DAO;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.stream.Stream;

import static org.mockito.Mockito.mock;

class ThreadDAOTests {
    @Nested
    @DisplayName("treeSort tests")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class TreeSortTests {
        Stream<Arguments> arguments() {
            return Stream.of(
                Arguments.of(2, null, 56, true, "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC ;"),
                Arguments.of(3, 13, 37, false, "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;")
            );
        }

        @ParameterizedTest
        @DisplayName("treeSort produces proper SQL")
        @MethodSource("arguments")
        void testSetPostSQL(Integer id, Integer limit, Integer since, Boolean desc, String sql) {
            JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
            new ThreadDAO(mockJdbc);
            ThreadDAO.treeSort(id, limit, since, desc);
            Mockito.verify(mockJdbc).query(Mockito.eq(sql), Mockito.any(PostDAO.PostMapper.class), Mockito.any());
        }
    }
}

